## What success this milestone looks like


## Issues

### Key

<!-- Key issues that are prioritized in this milestone -->

### Other

<!-- Hopefull there's a better name for this section in the future -->

## Ideal weights

<!-- This section contains estimates for the ideal weight as the team develops a velocity -->

## Team PTO

| Team Member      | days | When |
| ---------------- | ---- | ---- |
| `@godfat-gitlab` | <>   | <>   |
| `@kwiebers`      | <>   | <>   |
| `@rymai`         | <>   | <>   |
| `@ashmckenzie`   | <>   | <>   |
| `@ddieulivol`    | <>   | <>   |

## Historical Planning and Metrics
