## Engineering Productivity

This project exists to facilitate team planning activities. This includes OKR and Milestone planning.

Team direction will be created with epics under https://gitlab.com/gitlab-com/quality/

- Handbook: https://about.gitlab.com/handbook/engineering/quality/engineering-productivity/
